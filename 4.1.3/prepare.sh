
bash javadoc-gen.sh

gpg -ab --yes --local-user 4CBAB7BED6F959FFA4F525B6D205EC92B352803A ./java-gtk-maven-4.1.3.jar
gpg -ab --yes --local-user 4CBAB7BED6F959FFA4F525B6D205EC92B352803A ./java-gtk-maven-4.1.3.pom
gpg -ab --yes --local-user 4CBAB7BED6F959FFA4F525B6D205EC92B352803A ./java-gtk-maven-4.1.3-sources.jar
gpg -ab --yes --local-user 4CBAB7BED6F959FFA4F525B6D205EC92B352803A ./java-gtk-maven-4.1.3-javadoc.jar

jar -cvf bundle.jar \
   java-gtk-maven-4.1.3.jar \
   java-gtk-maven-4.1.3.pom \
   java-gtk-maven-4.1.3-sources.jar \
   java-gtk-maven-4.1.3-javadoc.jar \
   java-gtk-maven-4.1.3.jar.asc \
   java-gtk-maven-4.1.3.pom.asc \
   java-gtk-maven-4.1.3-sources.jar.asc \
   java-gtk-maven-4.1.3-javadoc.jar.asc
