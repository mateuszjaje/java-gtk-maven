
javadoc_filename=NO_SOURCES_README.txt
sources_filename=NO_SOURCES_README.txt
rm -r temp || echo "nothing to delete"

mkdir temp

echo "This is a reverse engineered maven dependency. it should be used only as \`provided\` dependency." > ./temp/$javadoc_filename
jar cvf java-gtk-maven-4.1.3-javadoc.jar -C temp .
rm ./temp/$javadoc_filename

echo "This is a reverse engineered maven dependency. it should be used only as \`provided\` dependency." > ./temp/$sources_filename
jar cvf java-gtk-maven-4.1.3-sources.jar -C temp .
rm ./temp/$sources_filename

rm -r temp
